package it.testing.funtestapp.adapters;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.ListPreloader;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.ViewTarget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.testing.funtestapp.R;
import it.testing.funtestapp.mvp.model.NetworkImage;
import it.testing.funtestapp.utils.GlideRequest;
import it.testing.funtestapp.utils.GlideRequests;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ImageViewHolder>
        implements ListPreloader.PreloadSizeProvider<NetworkImage>,
        ListPreloader.PreloadModelProvider<NetworkImage> {

    private final GlideRequest<Drawable> requestBuilder;

    private List<NetworkImage> data = new ArrayList<>();
    private RequestOptions options;

    private Listener listener;
    private int[] actualDimensions;

    public ImageListAdapter(GlideRequests glideRequests, int cellSize) {
        options = new RequestOptions()
                .centerInside()
                .dontAnimate()
//                .override(cellSize)
                .placeholder(new ColorDrawable(Color.GRAY))
                .error(new ColorDrawable(Color.BLACK))
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        requestBuilder = glideRequests.asDrawable().apply(options);
        actualDimensions = new int[] {cellSize, cellSize};
    }

    public List<NetworkImage> getData() {
        return data;
    }

    public void setData(@NonNull List<NetworkImage> data) {
        this.data.clear();
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    public void addData(@NonNull NetworkImage item) {
        data.add(item);
    }

    public void addData(@NonNull List<NetworkImage> items) {
        data.addAll(items);
    }

    public void setListener(@NonNull Listener listener) {
        this.listener = listener;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_image, parent, false);
        v.getLayoutParams().width = actualDimensions[0];
        v.getLayoutParams().height = actualDimensions[0];
        v.requestLayout();
        ImageViewHolder vh = new ImageViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        NetworkImage image = getItem(position);
        ImageViewHolder vh = holder;
        ViewTarget<ImageView, Drawable> target = Glide.with(vh.itemView)
                .load(image.getUrl())
                .apply(options)
                .into(vh.image);
//                .clearOnDetach();
        vh.target = target;
    }

    public NetworkImage getItem(int position) {
        return data.get(position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @NonNull
    @Override
    public List<NetworkImage> getPreloadItems(int position) {
        NetworkImage img = data.get(position);
        if (TextUtils.isEmpty(img.getUrl())) {
            return Collections.emptyList();
        }
        return Collections.singletonList(img);
    }

    @Nullable
    @Override
    public RequestBuilder<?> getPreloadRequestBuilder(@NonNull NetworkImage item) {
        return requestBuilder
                .clone()
                .load(item.getUrl());
    }



    @Nullable
    @Override
    public int[] getPreloadSize(@NonNull NetworkImage item, int adapterPosition, int perItemPosition) {
        return actualDimensions;
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;
        public ViewTarget<ImageView, Drawable> target;

        public ImageViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                listener.onItemClicked(position);
                Log.d("HANDLER", "onClick: position - " + position);
            });
            image = itemView.findViewById(R.id.image);
        }
    }

    public interface Listener {
        void onItemClicked(int pos);
    }
}
