package it.testing.funtestapp.mvp.model;

import java.io.Serializable;
import java.util.UUID;

public class NetworkImage implements Serializable {
    private String id;
    private String url;
    private String fullSizeUrl;

    public NetworkImage() {
        this.id = UUID.randomUUID().toString();
    }

    public NetworkImage(String url) {
//        this.id = String.valueOf(Math.random() * 1000);
        this();
        setUrl(url);
    }

    public NetworkImage(String url, String fullSizeUrl) {
//        this.id = String.valueOf(Math.random() * 1000);
        this(url);
        setFullSizeUrl(fullSizeUrl);
    }

    public String getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullSizeUrl() {
        return fullSizeUrl;
    }

    public void setFullSizeUrl(String fullSizeUrl) {
        this.fullSizeUrl = fullSizeUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NetworkImage image = (NetworkImage) o;

        if (id != null ? !id.equals(image.id) : image.id != null) return false;
        return url != null ? url.equals(image.url) : image.url == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }
}
