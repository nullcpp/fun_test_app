package it.testing.funtestapp.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKPhotoArray;

import java.util.ArrayList;
import java.util.List;

import it.testing.funtestapp.mvp.model.NetworkImage;
import it.testing.funtestapp.mvp.view.MainView;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {

    private final int count = 40;
    private boolean isLoggedIn = false;
    private int page = 0;
    private int lastScrolledItemPosition = -1;

    private List<NetworkImage> images = new ArrayList<>(200);

    public MainPresenter() {
    }

    public void onLoginCompleted() {
        isLoggedIn = true;
        onLoadMorePhotos();
    }

    public void onLoginFailed(VKError error) {
        // TODO: Better error handling
        isLoggedIn = false;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void onScrollPositionChanged(int position) {
        lastScrolledItemPosition = position;
    }

    public void restoreScrollPosition() {
        getViewState().scrollToItemPosition(lastScrolledItemPosition);
    }

    public void onLoadMorePhotos() {
        VKRequest request = new VKRequest("photos.get",
                VKParameters.from(VKApiConst.OWNER_ID, -19822628,
                        VKApiConst.ALBUM_ID, 248793388,
                        VKApiConst.REV, 0,
                        VKApiConst.OFFSET, page * count,
                        VKApiConst.COUNT, count),
                VKPhotoArray.class);
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                super.onComplete(response);
                VKPhotoArray photoArray = (VKPhotoArray) response.parsedModel;
                List<NetworkImage> list =  new ArrayList<>();
                for (VKApiPhoto item : photoArray) {
                    NetworkImage img = new NetworkImage(item.photo_807, item.photo_1280);
                    list.add(img);
                }
                page += 1;
                images.addAll(list);
                getViewState().showNewPhotos(images);
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                getViewState().onLoadPhotoAttemptFailed();
                super.attemptFailed(request, attemptNumber, totalAttempts);
            }

            @Override
            public void onError(VKError error) {
                getViewState().onLoadPhotoError(error.errorMessage);
                super.onError(error);
            }
        });
    }
}
