package it.testing.funtestapp.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import java.util.List;

import it.testing.funtestapp.mvp.model.NetworkImage;

public interface MainView extends MvpView {
    @StateStrategyType(SkipStrategy.class)
    void prepareViews();
    @StateStrategyType(AddToEndSingleStrategy.class)
    void showNewPhotos(List<NetworkImage> list);
    void scrollToItemPosition(int position);

    @StateStrategyType(SkipStrategy.class)
    void onLoadPhotoError(String errorMessage);
    @StateStrategyType(SkipStrategy.class)
    void onLoadPhotoAttemptFailed();
}
