package it.testing.funtestapp.network;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import it.testing.funtestapp.BuildConfig;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

public class OkHttpProvider {
    private static final OkHttpProvider instance = new OkHttpProvider();

    public static OkHttpProvider getInstance() {
        return instance;
    }

    private OkHttpProvider() {
    }

    public OkHttpClient provideClient() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(provideGoogleApiAuthInterceptor())
                .addInterceptor(provideLoggingInterceptor())
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .build();
        return client;
    }

    private Interceptor provideGoogleApiAuthInterceptor() {
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();

                HttpUrl url = request.url();
                if (url.isHttps() && url.host().startsWith("www.googleapis.com")) {
                    HttpUrl preparedUrl = url.newBuilder()
                            .addQueryParameter("key", BuildConfig.GOOGLE_CUSTOM_SEARCH_KEY)
                            .addQueryParameter("cx", BuildConfig.GOOGLE_CUSTOM_SEARCH_INSTANCE_ID)
                            .build();
                    request = request.newBuilder().url(preparedUrl).build();
                }
                Response response = chain.proceed(request);

                return response;
            }
        };
        return interceptor;
    }

    private Interceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        return interceptor;
    }
}
