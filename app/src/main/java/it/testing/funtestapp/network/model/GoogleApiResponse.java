package it.testing.funtestapp.network.model;

import java.util.List;

public class GoogleApiResponse {
    List<GoogleApiItem> items;

    public GoogleApiResponse() {
    }

    public List<GoogleApiItem> getItems() {
        return items;
    }

    public void setItems(List<GoogleApiItem> items) {
        this.items = items;
    }

    public static class GoogleApiItem {
        private GoogleApiImage image;
        private String link;

        public GoogleApiItem() {
        }

        public GoogleApiImage getImage() {
            return image;
        }

        public void setImage(GoogleApiImage image) {
            this.image = image;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }
    }

    public static class GoogleApiImage {
        private String contextLink;     // "https://www.homedepot.com/b/Outdoors-Garden-Center-Trees-Bushes/N-5yc1vZc8rq",
        private int height;             // 400,
        private int width;              // 400,
        private int byteSize;          // 124469,
        private String thumbnailLink;   // "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiySjdawAPcZmgbzT6g9cnJaYitqfioydstV9s8usJUK2hGiWLxQ8q1g",
        private int thumbnailHeight;    // 124,
        private int thumbnailWidth;     // 124
    }
}
