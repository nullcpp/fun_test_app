package it.testing.funtestapp.network;


import io.reactivex.Single;
import it.testing.funtestapp.network.model.GoogleApiResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleImagesApi {

    @GET("/customsearch/v1?" +
            "&searchType=image")
    Call<GoogleApiResponse> fetchImages(@Query("q") String query,
                                        @Query("start") int startCount,
                                        @Query("num") int count);
    @GET("/customsearch/v1?" +
            "&searchType=image")
    Single<GoogleApiResponse> fetchImagesAsync(@Query("q") String query,
                                               @Query("start") int startCount,
                                               @Query("num") int count);
}
