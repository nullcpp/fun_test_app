package it.testing.funtestapp.network;

import android.content.Context;

import io.reactivex.Single;
import it.testing.funtestapp.network.model.GoogleApiResponse;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkService {
    private static volatile NetworkService instance;
    private static final Object lock = new Object();

    private GoogleImagesApi api;

    private NetworkService(Context context) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://www.googleapis.com/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
                .addConverterFactory(GsonConverterFactory.create())
                .client(OkHttpProvider.getInstance().provideClient())
                .build();
        GoogleImagesApi googleImagesApi = retrofit.create(GoogleImagesApi.class);
        api = googleImagesApi;
    }

    public Call<GoogleApiResponse> search(String query, int start, int count) {
        Call<GoogleApiResponse> call = api.fetchImages(query, start, count);
        return call;
    }

    public Single<GoogleApiResponse> searchAsync(String query, int start, int count) {
        Single<GoogleApiResponse> call = api.fetchImagesAsync(query, start, count);
        return call;
    }

    //  Main
    public static NetworkService with(Context context) {
        if (instance == null) {
            synchronized (lock) {
                if (instance == null) {
                    instance = new NetworkService(context);
                }
            }
        }
        return instance;
    }

}
