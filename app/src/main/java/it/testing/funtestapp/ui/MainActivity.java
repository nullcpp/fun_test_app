package it.testing.funtestapp.ui;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.integration.recyclerview.RecyclerViewPreloader;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import java.util.List;

import it.testing.funtestapp.R;
import it.testing.funtestapp.adapters.ImageListAdapter;
import it.testing.funtestapp.databinding.ActivityMainBinding;
import it.testing.funtestapp.mvp.model.NetworkImage;
import it.testing.funtestapp.mvp.presenter.MainPresenter;
import it.testing.funtestapp.mvp.view.MainView;
import it.testing.funtestapp.utils.GlideApp;
import it.testing.funtestapp.utils.GlideRequests;

public class MainActivity extends MvpAppCompatActivity implements MainView {

    private static final String TAG = MainActivity.class.getCanonicalName();

    @InjectPresenter
    MainPresenter presenter;

    private ActivityMainBinding ui;
    private ImageListAdapter adapter;
    private final int spanCount = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ui = DataBindingUtil.setContentView(this, R.layout.activity_main);
        if (!presenter.isLoggedIn() || !VKSdk.isLoggedIn()) {
            VKSdk.login(this, "photos");
        }
        initImageList();
        initImageListAdapter();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                presenter.onLoginCompleted();
                // User passed Authorization
            }
            @Override
            public void onError(VKError error) {
                presenter.onLoginFailed(error);
                // User didn't pass Authorization
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void prepareViews() {
        presenter.onLoadMorePhotos();
    }

    @Override
    public void showNewPhotos(List<NetworkImage> list) {
        adapter.setData(list);
    }

    @Override
    public void onLoadPhotoError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoadPhotoAttemptFailed() {
        Toast.makeText(this, R.string.error_attempt_load_photo, Toast.LENGTH_LONG).show();
    }

    @Override
    public void scrollToItemPosition(int position) {
        ui.list.scrollToPosition(position);
    }

    private void initImageListAdapter() {
        GlideRequests glideRequests = GlideApp.with(this);
        adapter = new ImageListAdapter(glideRequests, calculateCellSize(spanCount));
        adapter.setListener(pos -> {
            NetworkImage item = adapter.getItem(pos);
            Intent intent = ImageFullscreenActivity.intent(MainActivity.this, item);
            startActivity(intent);
        });
        ui.list.setAdapter(adapter);
        ui.list.setHasFixedSize(true);
        ui.list.setItemViewCacheSize(20);
        ui.list.setDrawingCacheEnabled(true);
    }

    private void setupImagePreloader() {
        GlideRequests glideRequests = GlideApp.with(this);
        RecyclerViewPreloader<NetworkImage> preloader =
                new RecyclerViewPreloader<>(glideRequests, adapter, adapter, 40);
        ui.list.addOnScrollListener(preloader);
    }

    private void initImageList() {
        final GridLayoutManager layoutManager = new GridLayoutManager(this, spanCount);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (adapter.getItemViewType(position)) {
                    case 0:
                        return 1;
                    case 1:
                        return spanCount;
                    default:
                        return -1;
                }
            }
        });
        ui.list.setItemAnimator(new DefaultItemAnimator());
        ui.list.setLayoutManager(layoutManager);

        initImageListScroll(layoutManager);
    }

    private void initImageListScroll(GridLayoutManager layoutManager) {
        final ViewTreeObserver viewTreeObserver = ui.list.getViewTreeObserver();
        viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                setupImagePreloader();
                ui.list.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        ui.list.addOnScrollListener(new RecyclerView.OnScrollListener() {

            private int visibleThreshold = -1;
            int previousTotal = 0;
            boolean loading = true;

            int firstVisibleItem, lastVisibleItem, visibleItemCount, totalItemCount;

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                firstVisibleItem = layoutManager.findFirstVisibleItemPosition();
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
                visibleItemCount = ui.list.getChildCount();
                totalItemCount = layoutManager.getItemCount();

                if (visibleThreshold == -1) {
                    visibleThreshold = lastVisibleItem - firstVisibleItem;
                }

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    onLoadMore();
                    loading = true;
                }
            }
        });
    }

    private void onLoadMore() {
        presenter.onLoadMorePhotos();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private int calculateCellSize(int cellCount) {
        int cellSize = (int) Math.floor(getScreenWidth(this) / cellCount);
        return cellSize;
    }

    private float convertDPToPixels(int dp) {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float logicalDensity = metrics.density;
        return dp * logicalDensity;
    }

    @SuppressWarnings("deprecation")
    private static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }
}

